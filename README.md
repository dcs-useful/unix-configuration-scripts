# Shell Scripts for Setting Up Unix Systems

It works for me. Use it at your peril. This repo contains scripts to configure unix systems so they look pretty! :D

### Done:
 - Vim configuration script
 - Bash configuration script

### To-Do:
 - Python install script
 - Docker install script
