#!/bin/fish

# Install fisher
curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish

# Install Powerline Font
mkdir -p /tmp
git clone https://github.com/powerline/fonts.git /tmp/fonts --depth=1 
cd /tmp/fonts && bash install.sh
cd ~
rm -rf /tmp/fonts

# Install Spacefish
fisher add matchai/spacefish

# Install SSH Agent Functions
wget https://gitlab.com/kyb/fish_ssh_agent/raw/master/functions/fish_ssh_agent.fish -P ~/.config/fish/functions/

mkdir -p ~/.config/fish
sh -c "cat << 'EOF' > ~/.config/fish/config.fish
# Aliases
alias ..='cd ..'
alias ...='cd ..; cd ..'
alias ....='cd ..; cd ..; cd ..'
alias .....='cd ..; cd ..; cd ..;cd ..'
alias ll='ls -hlaG'
alias ls='ls -hG'
alias v='vim'
alias c='cat'
alias d='docker'
alias dc='docker compose'
alias g='git'
alias gst='git status'
alias gcm='git commit -m'
alias gad='git add -p'
alias gcl='git clone'
alias glg='git log'
alias gbr='git branch'
alias gch='git checkout'
alias gpl='git pull'
alias gps='git push'
alias gdif='git diff'
alias gcf='git config'
alias gcfem='git config --local user.email'
alias meow='cat'
alias wsp='cd ~/Workspace'
alias t='terraform'

# Editor Config
export EDITOR=vim

# Directory Coloring
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

##############
# SPACEFISH! #
##############
set SPACEFISH_DIR_TRUNC 3
set SPACEFISH_DIR_TRUNC_REPO false

# Colours (https://fishshell.com/docs/current/index.html#variables-color)
set fish_color_normal white # the default color
set fish_color_command white --bold # the color for commands
set fish_color_quote yellow # the color for quoted blocks of text
set fish_color_redirection '2EE' # the color for IO redirections
set fish_color_end magenta # the color for process separators like ';' and '&'
set fish_color_error 'f92' # the color used to highlight potential errors
set fish_color_param 'FFF' # the color for regular command parameters
set fish_color_comment green # the color used for code comments
set fish_color_match 'CCC' # the color used to highlight matching parenthesis
set fish_color_selection --background '333' # the color used when selecting text (in vi visual mode)
set fish_color_search_match 'D2D' # used to highlight history search matches and the selected pager item (must be a background)
set fish_color_operator 'FFF' # the color for parameter expansion operators like '*' and '~'
set fish_color_escape white --bold  # the color used to highlight character escapes like '\n' and '\x70'
set fish_color_cwd cyan # the color used for the current working directory in the default prompt
set fish_color_autosuggestion '999' # the color used for autosuggestions
set fish_color_user green # the color used to print the current username in some of fish default prompts
set fish_color_host green # the color used to print the current host system in some of fish default prompts
set fish_color_cancel brred # the color for the '^C' indicator on a canceled command
set fish_pager_color_prefix white # the color of the prefix string # i.e. the string that is to be completed
set fish_pager_color_completion 'DDD' # the color of the completion itself
set fish_pager_color_description 'DD2' # the color of the completion description
set fish_pager_color_progress default # the color of the progress bar at the bottom left corner
set fish_pager_color_secondary default # the background color of the every second completion
set -g fish_user_paths '/usr/local/opt/sqlite/bin' $fish_user_paths
set -g fish_user_paths '/usr/local/opt/openssl/bin' $fish_user_paths

# # SSH Keys
# eval (ssh-agent -c) > /dev/null
# ssh-add -l > /dev/null || ssh-add
# ssh-add -K (HOME)/.ssh/key
EOF"
