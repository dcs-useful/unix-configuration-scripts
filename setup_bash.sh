#!/bin/sh

cd ~
mkdir -p ~/Workspace

if [ -f ~/.bashrc ]; then
    cp ~/.bashrc ~/.old_bashrc
fi

cat << 'EOF' > ~/.bashrc
# Author: Dean Shanahan

# Aliases
alias ..='cd ..'
alias ...='cd ..; cd ..'
alias ....='cd ..; cd ..; cd ..'
alias ll='ls -hlaG'
alias ls='ls -hG'
alias v='vim'
alias c='cat'
alias d='docker'
alias dc='docker compose'
alias g='git'
alias gst='git status'
alias gcm='git commit -m'
alias gad='git add -p'
alias gcl='git clone'
alias glg='git log'
alias gbr='git branch'
alias gch='git checkout'
alias gpl='git pull'
alias gps='git push'
alias gdif='git diff'
alias meow='cat'
alias wsp='cd ~/Workspace'

# Editor Config
export EDITOR=vim
export PS1='\[\e[92m\]\u \[\e[97m\]-> \[\e[96m\]$(case $PWD in
        $HOME) HPWD="~";;
        $HOME/*/*) HPWD="../${PWD#"${PWD%/*/*}/"}";;
        $HOME/*) HPWD="~/${PWD##*/}";;
        /*/*/*) HPWD="${PWD#"${PWD%/*/*}/"}";;
        *) HPWD="$PWD";;
      esac; printf %s "$HPWD")\[\e[97m\]$ '
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

EOF


# Find all ssh keys and add them to the ssh-agent
if [ -f "$HOME/.ssh/*.pub" ]; then
    cat << 'EOF' > ~/.bashrc
    # SSH Keys
    if [ ! -S ~/.ssh/ssh_auth_sock ]; then
    eval `ssh-agent`
    ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
    fi
    export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
    ssh-add -l > /dev/null || ssh-add
EOF

    find ~/.ssh -iname "*.pub" | sed 's/\.pub$//g' | while read line; do
        echo "ssh-add -K $line" >> ~/.bashrc
    done
fi

