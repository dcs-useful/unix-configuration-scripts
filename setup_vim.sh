#!/bin/sh

# Author: Dean Shanahan
# Description: Script for installing vim packages and settin gup .vimrc
#

# Temporary directory for install files
mkdir -p ~/.vim/temp

# Set pull vim colorscheme
cd ~/.vim/temp
git clone https://github.com/tomasr/molokai.git
mkdir -p ~/.vim/colors && cp -r molokai/colors/molokai.vim ~/.vim/colors/
rm -rf molokai

cd ~/.vim/temp
git clone https://github.com/rakr/vim-one.git
mkdir -p ~/.vim/colors && cp -r vim-one/colors/one.vim ~/.vim/colors/
rm -rf vim-one

# Install pathogen
cd ~/.vim/temp
git clone https://github.com/tpope/vim-pathogen.git
mkdir -p ~/.vim/autoload && cp -r vim-pathogen/autoload/pathogen.vim ~/.vim/autoload/
rm -rf vim-pathogen


##### INSTALL USING PATHOGEN #####

# Useful when using git
cd ~/.vim/temp
git clone https://github.com/airblade/vim-gitgutter.git
rm -rf vim-gitgutter/.git
mkdir -p ~/.vim/bundle
cp -r vim-gitgutter ~/.vim/bundle/
rm -rf vim-gitgutter

# Syntax highlighting for Jenkinsfiles
cd ~/.vim/temp
git clone https://github.com/martinda/Jenkinsfile-vim-syntax.git
rm -rf Jenkinsfile-vim-syntax/.git
mkdir -p ~/.vim/bundle
cp -r Jenkinsfile-vim-syntax ~/.vim/bundle/
rm -rf Jenkinsfile-vim-syntax

# Asynchronous Lint Engine (Whoop whoop!)
cd ~/.vim/temp
git clone https://github.com/w0rp/ale.git
rm -rf ale/.git
mkdir -p ~/.vim/bundle
cp -r ale ~/.vim/bundle/
rm -rf ale

# Syntax highlighting for Terraform
cd ~/.vim/temp
git clone https://github.com/hashivim/vim-terraform.git
rm -rf vim-terraform/.git
mkdir -p ~/.vim/bundle
cp -r vim-terraform ~/.vim/bundle/
rm -rf vim-terraform

# Lightline (new statusline)
cd ~/.vim/temp
git clone https://github.com/itchyny/lightline.vim
rm -rf lightline.vim/.git
mkdir -p ~/.vim/bundle
cp -r lightline.vim ~/.vim/bundle/
rm -rf lightline.vim

# Surround (a useful plugin for surrounding test with brackets or quotes)
cd ~/.vim/temp
git clone https://tpope.io/vim/surround.git
rm -rf surround/.git
mkdir -p ~/.vim/bundle
cp -r surround ~/.vim/bundle/
rm -rf surround

# Fugitive (use git from vim)
cd ~/.vim/temp
git clone https://github.com/tpope/vim-fugitive.git
rm -rf vim-fugitive/.git
mkdir -p ~/.vim/bundle
cp -r vim-fugitive ~/.vim/bundle/
rm -rf vim-fugitive

# vim-go (go syntax highlighting for vim)
cd ~/.vim/temp
git clone https://github.com/fatih/vim-go.git
rm -rf vim-go/.git
mkdir -p ~/.vim/bundle
cp -r vim-go ~/.vim/bundle/
rm -rf vim-go

##### CUSTOM CONFIGURATION #####

# ~/.vimrc
cat <<'EOF' > ~/.vimrc
"Author: Dean Shanahan
"Description: .vimrc file for formatting the vim editor

execute pathogen#infect()

"Solve compatability issues and set colorscheme
set nocompatible
let g:lightline = {
        \ 'colorscheme': 'wombat',
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
        \ },
        \ 'component_function': {
        \   'gitbranch': 'fugitive#head'
        \ },
        \ }
colorscheme one "looks awesome!
set background=dark
syntax enable

"Tab Settings
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
filetype plugin indent on

"Editor Formatting
set number
set showcmd
filetype indent on
set wildmenu
set lazyredraw
set showmatch
set incsearch
set hlsearch
set ignorecase
set smartcase
nnoremap <leader><space> :nohlsearch<CR>
set foldenable
set foldlevelstart=10
nnoremap <space> za
set foldmethod=indent
set confirm

"Turn Off Backup Files
set noswapfile
set nobackup
set nowb

"Ensure backspace works
set backspace=indent,eol,start

"For lightline
set laststatus=2
EOF

# Allow fo filetype plugins
mkdir -p ~/.vim/ftplugin

# ~/.vim/ftplugin/yaml.vim
cat <<'EOF' > ~/.vim/ftplugin/yaml.vim
"Author: Dean Shanahan
"Description: A custom .vimrc for YAML files

"Tab Settings
setlocal tabstop=2
setlocal shiftwidth=2

"For yanking between terminals
set clipboard=unnamed
set clipboard^=unnamedplus

" Remove trailing white spaces on save
autocmd BufWritePre * :%s/\s\+$//e
EOF
